#ifndef dbg_raymarchingparams
#define dbg_raymarchingparams

#define STEPS 128
#define EPSILON 0.00001f
#define DELTA 0.00001f

#endif