using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatableSettings : ScriptableObject
{
    [SerializeField] protected bool autoUpdate;
}
