using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RaysSettings
{
    public ComputeShader computeShader;
    public string computeKernel;
}
